[TLU_0]
orientation = 0,0,0
orientation_mode = "xyz"
position = 0,0,0
role = "auxiliary"
time_resolution = 1ns
type = "tlu"

[MIMOSA26_0]
mask_file = "mask_MIMOSA26_0.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = -3.08228deg,-0.127025deg,-0.302694deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = 234.022um,73.06um,0
spatial_resolution = 4um,4um
time_resolution = 230us
type = "mimosa26"

[MIMOSA26_1]
mask_file = "mask_MIMOSA26_1.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = 4.85519deg,0.411785deg,-0.174294deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = -453.09um,365.497um,153mm
spatial_resolution = 4um,4um
time_resolution = 230us
type = "mimosa26"

[MIMOSA26_2]
mask_file = "mask_MIMOSA26_2.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = -5.90708deg,-0.571239deg,0.0260123deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = -1.11181mm,185.296um,305mm
spatial_resolution = 4um,4um
time_resolution = 230us
type = "mimosa26"

[ATLASPix_0]
mask_file = "mask_atlaspix.txt"
material_budget = 0.015
number_of_pixels = 25, 400
orientation = 178.189deg,-3.832deg,89.9415deg
orientation_mode = "xyz"
pixel_pitch = 130um,40um
position = 63.792um,22.531um,333mm
role = "dut"
spatial_resolution = 40um,10um
time_resolution = 7ns
type = "atlaspix"

[MIMOSA26_3]
mask_file = "mask_MIMOSA26_3.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = 6.25968deg,0.630368deg,0.0142094deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = -1.96859mm,414.063um,344mm
role = "reference"
spatial_resolution = 4um,4um
time_resolution = 230us
type = "mimosa26"

[MIMOSA26_4]
mask_file = "mask_MIMOSA26_4.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = -7.72502deg,-0.758653deg,-0.357182deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = -1.64107mm,321.948um,456mm
spatial_resolution = 4um,4um
time_resolution = 230us
type = "mimosa26"

[MIMOSA26_5]
mask_file = "mask_MIMOSA26_5.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = -8.95504deg,-0.809475deg,0.0648588deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = -1.31559mm,353.953um,576mm
spatial_resolution = 4um,4um
time_resolution = 230us
type = "mimosa26"

[Timepix3_0]
material_budget = 0.01068
number_of_pixels = 256, 256
orientation = 170.076deg,-0.705483deg,0.604872deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = -597.025um,338.269um,666mm
spatial_resolution = 10um,10um
time_resolution = 1.56ns
type = "timepix3"

