[Timepix3_0]
number_of_pixels = 256, 256
orientation = 9.87573deg,185.975deg,-0.801167deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = 1.13059mm,442.417um,0
spatial_resolution = 4um,4um
time_resolution = 1.5ns
type = "timepix3"

[Timepix3_1]
number_of_pixels = 256, 256
orientation = 10.2377deg,186.353deg,-0.588771deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = -68.144um,535.78um,21.5mm
spatial_resolution = 4um,4um
time_resolution = 1.5ns
type = "timepix3"

[Timepix3_2]
number_of_pixels = 256, 256
orientation = 9.60478deg,186.892deg,-1.22498deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = 207.869um,502.408um,43.5mm
spatial_resolution = 4um,4um
time_resolution = 1.5ns
type = "timepix3"

[Timepix3_3]
number_of_pixels = 256, 256
orientation = 9.00002deg,9.00002deg,0
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = 0,0,186.5mm
role = "reference"
spatial_resolution = 4um,4um
time_resolution = 1.5ns
type = "timepix3"

[Timepix3_4]
number_of_pixels = 256, 256
orientation = 7.33105deg,9.93606deg,1.07596deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = 435.153um,-600.791um,231.5mm
spatial_resolution = 4um,4um
time_resolution = 1.5ns
type = "timepix3"

[Timepix3_5]
number_of_pixels = 256, 256
orientation = 5.8389deg,10.8333deg,-0.752523deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = -1.02757mm,-59.28um,336.5mm
spatial_resolution = 4um,4um
time_resolution = 1.5ns
type = "timepix3"

