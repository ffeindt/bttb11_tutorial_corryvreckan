[Timepix3_0]
number_of_pixels = 256, 256
orientation = 10.2084deg,186.225deg,-0.840529deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = 1.12974mm,440.933um,0
spatial_resolution = 4um,4um
time_resolution = 1.5ns
type = "timepix3"

[Timepix3_1]
number_of_pixels = 256, 256
orientation = 10.5282deg,186.56deg,-0.625555deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = -68.504um,534.93um,21.5mm
spatial_resolution = 4um,4um
time_resolution = 1.5ns
type = "timepix3"

[Timepix3_2]
number_of_pixels = 256, 256
orientation = 9.87584deg,187.06deg,-1.25592deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = 207.318um,501.693um,43.5mm
spatial_resolution = 4um,4um
time_resolution = 1.5ns
type = "timepix3"

[ATLASpix_0]
number_of_pixels = 25, 400
orientation = -1.17032deg,-1.89414deg,-0.280234deg
orientation_mode = "xyz"
pixel_pitch = 130um,40um
position = 433.808um,-1.47398mm,105mm
roi = [[1,1],[1,398],[23,398],[23,1]]
role = "dut"
spatial_resolution = 40um,10um
time_resolution = 200ns
type = "atlaspix"

[Timepix3_3]
number_of_pixels = 256, 256
orientation = 9.00002deg,9.00002deg,0
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = 0,0,186.5mm
role = "reference"
spatial_resolution = 4um,4um
time_resolution = 1.5ns
type = "timepix3"

[Timepix3_4]
number_of_pixels = 256, 256
orientation = 7.36658deg,9.9014deg,1.0759deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = 435.067um,-600.873um,231.5mm
spatial_resolution = 4um,4um
time_resolution = 1.5ns
type = "timepix3"

[Timepix3_5]
number_of_pixels = 256, 256
orientation = 5.87081deg,10.7925deg,-0.75682deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = -1.02808mm,-59.319um,336.5mm
spatial_resolution = 4um,4um
time_resolution = 1.5ns
type = "timepix3"

